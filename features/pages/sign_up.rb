require 'rspec/expectations'
require 'csv'

class SignUp

	include RSpec::Matchers
	include PageObject	

	def visit_amazon_site
		browser.navigate.to "https://www.amazon.com"
	end

	def click_button_signin
		browser.find_element(:id, 'nav-link-accountList').click
	end

	def click_create_acct	
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 'createAccountSubmit').displayed? }
		browser.find_element(:id, 'createAccountSubmit').click
	end

	def input_name(text_name)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)
		wait.until { browser.find_element(:id, 'ap_customer_name').displayed? }
		browser.find_element(:id, 'ap_customer_name').clear
		browser.find_element(:id, 'ap_customer_name').send_keys text_name
	end

	def user_data
  		user_data = CSV.read Dir.pwd + '/features/support/user_data.csv'
  		descriptor = user_data.shift
  		descriptor = descriptor.map { |key| key.to_sym }
  		user_data.map { |user| Hash[ descriptor.zip(user) ] }
	end 



	def input_email(text_email)
		browser.find_element(:id, 'ap_email').send_keys text_email
	end

	def input_pwd(txt_pwd, txt_pwd_chck)
		browser.find_element(:id, 'ap_password').send_keys txt_pwd
		browser.find_element(:id, 'ap_password_check').send_keys txt_pwd_chck
	end

	def btn_continue
		browser.find_element(:id , 'continue').click
	end
end