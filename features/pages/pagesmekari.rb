require 'rspec/expectations'
require 'csv'

class PagesMekari

	include RSpec::Matchers
	include PageObject	

	def visit_mekari_site
		browser.navigate.to "https://sandbox.jurnal.id/"
	end

	def user_data
  		user_data = CSV.read Dir.pwd + '/features/support/user_data_mekari.csv'
  		descriptor = user_data.shift
  		descriptor = descriptor.map { |key| key.to_sym }
  		user_data.map { |user| Hash[ descriptor.zip(user) ] }
	end 

	def input_user(txt_email, txt_pwd)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 'user_email').displayed? }
		browser.find_element(:id, 'user_email').send_keys txt_email
		browser.find_element(:id, 'user_password').send_keys txt_pwd
		browser.find_element(:id, 'new-signin-button').click
		wait.until { browser.find_element(:id, 'vnav-sales-link').displayed? }
		browser.find_element(:id, 'vnav-sales-link').click
		wait.until { browser.find_element(:css, '.btn-dropdown-action').displayed? }
		browser.find_element(:css, '.btn-dropdown-action').click
		wait.until { browser.find_element(:xpath, "//a[@href='/invoices/new']").displayed? }
		browser.find_element(:xpath, "//a[@href='/invoices/new']").click
	end

	def select_cust
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 's2id_transaction_person_id').displayed? }
		browser.find_element(:id, 's2id_transaction_person_id').click
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:class, "select2-result-label").click
	end

	def add_new_cust(text_name,text_email,txt_addr,txt_phone)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 's2id_transaction_person_id').displayed? }
		browser.find_element(:id, 's2id_transaction_person_id').click
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:class, "add_new_product_link_child").click
		wait.until { browser.find_element(:name, "person[display_name]").displayed? }
		browser.find_element(:name, 'person[display_name]').send_keys text_name
		browser.find_element(:id, 'person_email').send_keys text_email
		browser.find_element(:id, 'person_billing_address').send_keys txt_addr
		browser.find_element(:id, 'person_phone').send_keys txt_phone
		browser.find_element(:id, 'aodc-add').click
	end

	def input_date(txt_date, txt_endate)
		browser.find_element(:id, 'transaction_transaction_date').clear
		browser.find_element(:id, 'transaction_transaction_date').send_keys txt_date
		browser.find_element(:id, 'transaction_due_date').clear
		browser.find_element(:id, 'transaction_due_date').send_keys txt_date
	end

	def input_from_table
		browser.find_element(:id, 'transaction_due_date').click
		wait = Selenium::WebDriver::Wait.new(:timeout => 15)
		table = wait.until {
    		element = browser.find_element(:class, "table-condensed")
    		element if element.displayed?
		}

		browser.find_elements(:xpath => "//table[@class='table-condensed']/tbody/tr/td").each do |r|
   		puts "Cell Value: " + r.text
   		end
	
	end	

	def select_product 
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 's2id_transaction_transaction_lines_attributes_0_product_id').displayed? }
		browser.find_element(:id, 's2id_transaction_transaction_lines_attributes_0_product_id').click
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:xpath, "//*[text()= 'Sales']").click
	end

	def add_new_product(txt_prod_name,txt_prod_code)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 's2id_transaction_transaction_lines_attributes_0_product_id').displayed? }
		browser.find_element(:id, 's2id_transaction_transaction_lines_attributes_0_product_id').click
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:class, "add_new_product_link_child").click
		wait.until { browser.find_element(:id, "product_name").displayed? }
		browser.find_element(:id, "product_name").send_keys txt_prod_name
		browser.find_element(:id, "product_product_code").click
		browser.find_element(:id, "product_product_code").send_keys txt_prod_code
		wait.until { browser.find_element(:id, 's2id_product_unit_id').displayed? }
		browser.find_element(:id, 's2id_product_unit_id').click
		browser.find_element(:id, 's2id_autogen26_search').click
		browser.find_element(:id, 's2id_autogen26_search').send_keys "Pcs"
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:xpath, '//*[starts-with(@class, "select2-result-label")]').click
		browser.find_element(:id, 'add-new-product').click	
	end

	def add_new_product_and_buy(txt_prod_name,txt_prod_code)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 's2id_transaction_transaction_lines_attributes_0_product_id').displayed? }
		browser.find_element(:id, 's2id_transaction_transaction_lines_attributes_0_product_id').click
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:class, "add_new_product_link_child").click
		wait.until { browser.find_element(:id, "product_name").displayed? }
		browser.find_element(:id, "product_name").send_keys txt_prod_name
		browser.find_element(:id, "product_product_code").click
		browser.find_element(:id, "product_product_code").send_keys txt_prod_code
		wait.until { browser.find_element(:id, 's2id_product_unit_id').displayed? }
		browser.find_element(:id, 's2id_product_unit_id').click
		browser.find_element(:id, 's2id_autogen26_search').click
		browser.find_element(:id, 's2id_autogen26_search').send_keys "Pcs"
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:xpath, '//*[starts-with(@class, "select2-result-label")]').click
		browser.find_element(:id , 'product_buy_price_per_unit').send_keys "100000"
		browser.find_element(:id, 's2id_product_buy_account_id').click
		browser.find_element(:id, 's2id_autogen10_search').send_keys "(5-50400) - Import Charges (Cost of Sales)"
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:xpath, '//*[starts-with(@class, "select2-result-label")]').click
		browser.find_element(:id , 's2id_product_buy_tax_id').click
		wait.until { browser.find_element(:class, "select2-result-label").displayed? }
		browser.find_element(:xpath, '//*[starts-with(@class, "select2-result-label")]').click
		browser.find_element(:id, 'add-new-product').click	
	end
	def add_new_product_and_buy(txt_amount)
		browser.find_element(:id, 'transaction_transaction_lines_attributes_0_rate').send_keys txt_amount
	end

	def add_attach
		filename = '/features/support/user_data_mekari.csv'
  		file = File.join(Dir.pwd, filename)
  		puts":" +file
		element = browser.find_element(:id, 'dropzoneAdd')
	end

	def create_button
		browser.find_element(:id, 'create_button').click
	end

	def search_result_present?
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
    	wait.until { browser.find_element(:xpath, '//*[starts-with(@class, "page-title-heading")]').displayed? }
   	 	element = browser.find_element(:xpath, '//*[starts-with(@class, "page-title-heading")]').text
   	 	puts ":" +element
 	end

	
end