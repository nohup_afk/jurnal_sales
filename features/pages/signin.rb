class SignIn

	include PageObject

	def input_user(txt_user)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 'ap_email').displayed? }
		browser.find_element(:id, 'ap_email').send_keys txt_user
	end

	def click_continue
		browser.find_element(:id, 'continue').click
	end

	def input_pwd(txt_pwd)
		wait = Selenium::WebDriver::Wait.new(:timeout => 10)	
		wait.until { browser.find_element(:id, 'ap_password').displayed? }
		browser.find_element(:id, 'ap_password').send_keys txt_pwd
	end

	def btn_sign_id
		browser.find_element(:id, 'signInSubmit').click
	end
end