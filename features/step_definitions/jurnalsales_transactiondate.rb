require_relative '../pages/pagesmekari.rb'

Given("Open Mekari homepage and select jurnal sales") do
	@jurnalsales = PagesMekari.new(@browser)
  	@jurnalsales.visit_mekari_site
  	  	@jurnalsales.user_data.each do |user|
			@jurnalsales.input_user(user[:email],user[:password])
	end
	@jurnalsales.select_cust
end


When("Select transaction date by inputting date") do
	@jurnalsales.input_date("20/04/2021","22/04/2021")

end

And("Select transaction date by selection date modal") do
	@jurnalsales.input_from_table
end

And("Select dropdown existing product on the list") do
	@jurnalsales.select_product
end

