require_relative '../pages/sign_up.rb'
require_relative '../pages/signin.rb'

Given("Navigate to amazon homepage") do
	@amazon_page = SignUp.new(@browser) 
  	@amazon_page.visit_amazon_site
end

When("user click button sign up") do
	@amazon_page.click_button_signin
end

And("user input email or phone number") do
	@login = SignIn.new(@browser)
	@amazon_page.user_data.each do |user|
		@login.input_user(user[:email])
	end
	
end

When("User click continue and user input password") do
	@login.click_continue
		@amazon_page.user_data.each do |user|
		@login.input_pwd(user[:password])
	end
end

Then("click sign-in") do
	@login.btn_sign_id
end
