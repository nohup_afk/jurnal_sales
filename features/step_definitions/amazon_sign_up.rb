require_relative '../pages/sign_up.rb'
 


Given("open amazon homepage") do
	@amazon_page = SignUp.new(@browser) 
  	@amazon_page.visit_amazon_site
end

When("a user click button sign up") do
	@amazon_page.click_button_signin
end

And("user click button create account") do
	@amazon_page.click_create_acct
end

When /^User input field create account$/ do 
	# @amazon_page.input_name("Jamsheedtherpg")
	# @amazon_page.input_email("vmuslmadel5@gmail.com")
	# @amazon_page.input_pwd("randomBoys69","randomBoys69")
		@amazon_page.user_data.each do |user|
		@browser.find_element(:id, 'ap_customer_name').send_keys user[:username]
   		@browser.find_element(id: 'ap_email').send_keys user[:email]
    	@browser.find_element(id: 'ap_password').send_keys user[:password]
    	@browser.find_element(:id, 'ap_password_check').send_keys user[:repwd]
	end
end

Then /^click button create account$/ do
	@amazon_page.btn_continue
end
