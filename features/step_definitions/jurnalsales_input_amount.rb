require_relative '../pages/pagesmekari.rb'

Given("Goto Mekari homepage and select jurnal sales") do
	@jurnalsales = PagesMekari.new(@browser)
  	@jurnalsales.visit_mekari_site
  	  	@jurnalsales.user_data.each do |user|
			@jurnalsales.input_user(user[:email],user[:password])
	end
	@jurnalsales.select_cust
end

When("Input unit price") do 
	@jurnalsales.select_product
	@jurnalsales.add_new_product_and_buy("100000")
end

And("Input attach") do 
	@jurnalsales.add_attach
end

Then("Click Create") do
	@jurnalsales.create_button
	@jurnalsales.search_result_present? 
	sleep(900)
end

