require_relative '../pages/pagesmekari.rb'

Given("Go Mekari homepage and select jurnal sales") do
	@jurnalsales = PagesMekari.new(@browser)
  	@jurnalsales.visit_mekari_site
  	  	@jurnalsales.user_data.each do |user|
			@jurnalsales.input_user(user[:email],user[:password])
	end
	@jurnalsales.select_cust
end

When("Select dropdown existing current product on the list") do 
	@jurnalsales.select_product
end