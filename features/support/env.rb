require 'selenium-webdriver'
require 'page-object'



options = Selenium::WebDriver::Chrome::Options.new
options.add_argument('--ignore-certificate-errors')
options.add_argument('--disable-popup-blocking')
options.add_argument('--disable-translate')

caps = Selenium::WebDriver::Remote::Capabilities.chrome(:chrome_options => {detach: true})

Before do |scenario|
  @browser = Selenium::WebDriver.for :chrome, options: options , desired_capabilities: caps
end

